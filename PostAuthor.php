<?php

namespace Stitchdx\WordPress\Traits;

/**
 * Class PostAuthor
 *
 * @package Stitchdx\WordPress\Traits
 *
 * @property \WP_Post $post
 */
trait PostAuthor {

	/**
	 * Get the author user object.
	 *
	 * @return \WP_User|false
	 */
	public function postAuthor() {
		return get_user_by( 'id', $this->post->post_author );
	}

	/**
	 * Get the author ID.
	 *
	 * @return int
	 */
	public function postAuthorId() {
		return absint( $this->post->post_author );
	}

	/**
	 * Get the author name.
	 *
	 * @return string
	 */
	public function postAuthorName() {
		$user = $this->postAuthor();

		return $user ? $user->display_name : '';
	}

}